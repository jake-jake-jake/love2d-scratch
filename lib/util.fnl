(fn table-to-string [t]
  "Translate table to pretty printed string representation."
  (var acc "")
  (each [k v (pairs t)]
        (set acc (.. acc k ": " (tostring v) "\n")))
  acc)

(fn angle-to-coordinate-speed [angle speed]
  (let [x-speed (* (math.cos angle) speed)
        y-speed (* (math.sin angle) speed)]
    [x-speed y-speed]))

;; (macro wrap-graphics [w h body ...]
;;   "Graphics drawn in this expression will be wrapped around the canvas"
;;   (assert w "must provide width to wrap")
;;   (assert h "must provide height to wrap")
;;   `(for [y -1 1]
;;      (for [x -1 1]
;;        (love.graphics.origin)
;;        (love.graphics.translate (* x w)
;;                                 (* y h))
;;        ,body
;;        ,...)))

{:table-to-string table-to-string
 :angle-to-coordinate-speed angle-to-coordinate-speed}
