(require "math")
(var shapes [])


(fn render-rectangle [rec]
  (love.graphics.rectangle (. rec "style")
                           (. rec "x")
                           (. rec "y")
                           (. rec "width")
                           (. rec "height")))
(fn render-circle [circ]
  (love.graphics.setColor 0 0 1)
  (love.graphics.circle (. circ "style")
                        (. circ "x")
                        (. circ "y")
                        (. circ "radius")))
(fn render-error [obj]
  (print "Bad object type")
  (print obj)
  (assert false))

(fn render [obj]
  (match (. obj "type")
    "rectangle" (render-rectangle obj)
    "circle"    (render-circle obj)
    _           (render-error obj)))

(fn get-coord []
  (math.random 0 400))

(fn get-size []
  (math.random 1 100))

(fn get-style []
  (let [love-styles [:line :fill]]
  (. love-styles (math.random 1 2))))

(fn get-rect [x y width height style]
  {:type   :rectangle
   :x      (or x      (get-coord))
   :y      (or y      (get-coord))
   :width  (or width  (get-size))
   :height (or height (get-size))
   :style  (or style  (get-style))})

(fn get-circ [x y radius style]
  {:type   :circle
   :x      (or x      (get-coord))
   :y      (or y      (get-coord))
   :radius (or radius (get-size))
   :style  (or style  (get-style))})

(fn add-rect []
  (table.insert shapes (get-rect)))

(fn add-circ []
  (table.insert shapes (get-circ)))

(fn do-updates [dt set-mode]
  )
{:draw       (fn draw []
               (each [_ item (ipairs shapes)]
                 (render item)))
 :update     (fn update [dt set-mode]
               )
 :keypressed (fn keypressed [key set-mode]
               (match key
                 "r" (add-rect)
                 "c" (add-circ)
                 "q" (love.event.quit))
                 _ nil)}
