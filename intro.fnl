(var counter 0)
(var time 0)

(local games {:a "Asteroids"
              :q "Quit"})


{:draw (fn draw [message]
         (love.graphics.print "WHAT YOU WANT TO PLAY??")
         (each [k v (pairs games)]
           love.graphics.print k v)
 :update (fn update [dt set-mode]

             ))
 :keypressed (fn keypressed [key set-mode]
                (match key
                  "a" (set-mode "asteroids")
                  "q" (love.event.quite)))}
