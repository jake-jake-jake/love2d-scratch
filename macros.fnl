(fn wrap-graphics [w h body ...]
  "Graphics drawn in this expression will be wrapped around the canvas"
  (assert w "must provide width to wrap")
  (assert h "must provide height to wrap")
  `(for [y# -1 1]
     (for [x# -1 1]
       (love.graphics.origin)
       (love.graphics.translate (* x# ,w)
                                (* y# ,h))
       ,body
       ,...)))

{:wrap-graphics wrap-graphics}
