(require "math")
(local util (require "lib.util"))
(import-macros {: wrap-graphics} :macros)

;; Game state
(local window (let [(w h) (love.window.getMode)]
                {:width w
                 :height h}))
(local colors
       {:ship         [0 0 1 1]
        :ship-pointer [0 1 1 1]
        :bullet       [0 1 0 1]
        :white        [1 1 1 1]
        :roids        [1 1 0 1]})

(local asteroid-stages
       [{:size  15
         :speed 120}
        {:size  30
         :speed 70}
        {:size  50
         :speed 50}
        {:size  80
         :speed 20}])


;; game oject creation functions
(fn make-ship []
  {:x          (/ (. window :width) 2)
   :y          (/ (. window :height) 2)
   :angle      0
   :size       15
   :speed 10
   :turn-speed 10
   :x-speed    0
   :y-speed    0
   ;; time to recharge
   :recharge   0
   :fire-delay .5})

(fn get-pointer-distance [size]
  (* 2
     (/ size 3)))

(fn get-pointer-size [ship]
  (/ (. ship :size) 5))

(fn make-pointer [ship]
  (let [distance (get-pointer-distance (. ship :size))]
    {:distance distance
     :x        (+ (. ship :x)
                  (* (math.cos (. ship :angle))
                     distance))
     :y        (+ (. ship :y)
                  (* (math.sin (. ship :angle))
                     distance))
     :size     (get-pointer-size ship)}))

(fn size-and-speed-by-stage [stage]
  (let [mults (. asteroid-stages stage)
        spd (. mults :speed)
        sze  (. mults :size)
        speed (love.math.random (- spd 5 ) (+ spd 5))
        size (love.math.random (- sze 5 ) (+ sze 5))]
    [size speed]))

(fn make-asteroid [x y ?stage]
  (let [angle    (* (love.math.random) 2 math.pi)
        stage    (or ?stage 4)
        [size speed] (size-and-speed-by-stage stage)
        [x-speed
         y-speed] (util.angle-to-coordinate-speed angle speed)]
    {:x       x
     :y       y
     :x-speed x-speed
     :y-speed y-speed
     :angle   angle
     :stage   stage
     :speed   speed
     :size    size}))

(fn initialize-roids []
  (let [roids []]
    (each [i [x y] (ipairs [[100 100]
                            [(- (. window :width) 100)
                             100]
                            [(/ (. window :width) 2)
                             (- (. window :height) 100)]])]
      (table.insert roids (make-asteroid x y)))
    roids))

(var ship (make-ship))
(var bullets [])
(var roids (initialize-roids))

(var debug false)
(fn toggle-debug []
  (set debug (not debug)))

(fn reset []
  (set ship (make-ship))
  (set bullets [])
  (set roids (initialize-roids)))

(fn make-bullet [ship]
  (let [delta-x   (* (. ship :size)
                     (math.cos (. ship :angle)))
        delta-y   (* (. ship :size)
                     (math.sin (. ship :angle)))
        [x-speed
         y-speed] (util.angle-to-coordinate-speed (. ship :angle)
                                                  300)]
    {:x       (+ delta-x (. ship :x))
     :y       (+ delta-y (. ship :y))
     :x-speed (+ x-speed (. ship :x-speed))
     :y-speed (+ y-speed (. ship :y-speed))
     :angle   (. ship :angle)
     :size    (get-pointer-size ship)
     :ttl     4}))

(fn shoot-bullet [ship]
  (if (> (. ship :recharge) (. ship :fire-delay))
      (let [bullet (make-bullet ship)]
        (table.insert bullets bullet)
        (tset ship :recharge 0))))

;; utility functions
(fn rotate-angle [angle
                  speed
                  clockwise?]
  (if clockwise?
      (% (+ angle speed) (* 2 math.pi))
      (% (- angle speed) (* 2 math.pi))))

(fn circles-collide? [circ1 circ2]
  "Returns true if circles have collided."
  (let [x1 (. circ1 :x)
        y1 (. circ1 :y)
        r1 (. circ1 :size)
        x2 (. circ2 :x)
        y2 (. circ2 :y)
        r2 (. circ2 :size)
        ;; if sum of difference of coordinates squared is less than
        ;; or equal to sum of radii squared, circles have collided
        rd (math.pow (+ r1 r2) 2)
        xd (math.pow (- x1 x2) 2)
        yd (math.pow (- y1 y2) 2)]
    (<= (+ xd yd) rd)))

;; Ship drawing functions
(fn draw-ship-debug []
  "Print out debug info on the ship..."
  (let [debug-item (. bullets 1)]
    (when debug-item
      (love.graphics.origin)
      (love.graphics.setColor (. colors :white))
      (love.graphics.print (util.table-to-string debug-item)))))

(fn draw-ship [ship]
  "Draw the ship"
  (love.graphics.setColor (. colors :ship))
  (love.graphics.circle "fill"
                        (. ship :x)
                        (. ship :y)
                        (. ship :size))
  (love.graphics.setColor (. colors :ship-pointer))
  (let [pointer (make-pointer ship)]
    (love.graphics.circle "fill"
                          (. pointer :x)
                          (. pointer :y)
                          (. pointer :size))))

(fn draw-bullets [bullets]
  (love.graphics.setColor (. colors :bullet))
  (each [i bullet (ipairs bullets)]
    (if (< 0 (. bullet :ttl))
        (love.graphics.circle "fill"
                              (. bullet :x)
                              (. bullet :y)
                              (. bullet :size))
        )
    ))
(fn break-roid [roid]
  (let [stage (. roid :stage)
        next-stage (- stage 1)
        x (. roid :x)
        y (. roid :y)]
    (when (> next-stage 0)
      (table.insert roids (make-asteroid x y next-stage))
      (table.insert roids (make-asteroid x y next-stage)))))
(fn draw-roids [roids]
  (love.graphics.origin)
  (love.graphics.setColor (. colors :roids))
  (each [i roid (ipairs roids)]
    (love.graphics.circle "fill"
                          (. roid :x)
                          (. roid :y)
                          (. roid :size))))

(fn move-body [body dt]
  (let [x-speed (* (. body   :x-speed) dt)
        y-speed (* (. body   :y-speed) dt)
        x       (. body   :x)
        y       (. body   :y)
        w       (. window :width)
        h       (. window :height)]
    (tset body :x (% (+ x x-speed) w))
    (tset body :y (% (+ y y-speed) h))))

(fn handle-input [dt]
  (if (love.keyboard.isDown "left")
      (let [angle (rotate-angle (. ship :angle)
                                (* (. ship :turn-speed) dt)
                                false)]
        (tset ship :angle angle)))
  (if (love.keyboard.isDown "right")
      (let [angle (rotate-angle (. ship :angle)
                                (* (. ship :turn-speed) dt)
                                true)]
        (tset ship :angle angle)))
  (if (love.keyboard.isDown "up")
      (let [[delta-x delta-y] (util.angle-to-coordinate-speed (. ship :angle)
                                                              (. ship :speed))]
        (tset ship :x-speed (+ (. ship :x-speed)
                               delta-x))
        (tset ship :y-speed (+ (. ship :y-speed)
                               delta-y))))
  (if (love.keyboard.isDown "space")
      (shoot-bullet ship)))

(fn update-state [dt]
  ;; update bullet state
  (var live-bullets [])
  (each [i bullet (ipairs bullets)]
    (let [ttl (- (. bullet :ttl)
                 dt)]
      (tset bullet :ttl ttl)
      (move-body bullet dt)
      (if (< 0 ttl)
          (table.insert live-bullets bullet)
          )))
  (set bullets live-bullets)

  ;; update ship state
  (move-body ship dt)
  (tset ship :recharge (+ (. ship :recharge) dt))

  ;; update roids
  (each [i roid (ipairs roids)]
    (move-body roid dt))

  ;; do collisions

  ;; this for loop is inefficient; dunno how do break with fennel
  (for [i (length bullets) 1 -1]
    (for [j (length roids) 1 -1]
      (let [bullet (. bullets i)
            roid   (. roids   j)]
        (when (and bullet
                   roid
                   (circles-collide? bullet
                                     roid))
          (break-roid roid)
          (table.remove bullets i)
          (table.remove roids j))
        )))
  (each [i roid (ipairs roids)]
    (if (circles-collide? ship roid)
        (reset))))

{:draw       (fn draw [dt set-mode]
               (wrap-graphics (. window :width) (. window :height)
                              (draw-ship ship)
                              (draw-bullets bullets)
                              (draw-roids roids))
               (draw-ship-debug))
 :update     (fn update [dt set-mode]
               (handle-input dt)
               (update-state dt))
 :keypressed (fn keypressed [key set-mode]
               (match key
                 "q" (love.event.quit)
                 "r" (reset)
                 "d" (toggle-debug)))}
